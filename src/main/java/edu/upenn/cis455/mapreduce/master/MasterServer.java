package edu.upenn.cis455.mapreduce.master;

import static spark.Spark.*;

public class MasterServer {  
    public static void registerStatusPage() {
        get("/status", (request, response) -> {
            response.type("text/html");

            return ("<html><head><title>Master</title></head>\n" +
                    "<body>Hi, I am the master!</body></html>");
        });

    }

    /**
     * The mainline for launching a MapReduce Master.  This should
     * handle at least the status and workerstatus routes, and optionally
     * initialize a worker as well.
     * 
     * @param args
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: MasterServer [port number]");
            System.exit(1);
        }

        int myPort = Integer.valueOf(args[0]);
        port(myPort);

        System.out.println("Master node startup, on port " + myPort);

        // TODO: you may want to adapt parts of edu.upenn.cis.stormlite.mapreduce.TestMapReduce here

        registerStatusPage();

        // TODO: route handler for /workerstatus reports from the workers
    }
}

